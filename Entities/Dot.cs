﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy.Entities
{
    public class Dot
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Dot(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

    }
}
