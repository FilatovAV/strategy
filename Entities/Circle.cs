﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy.Entities
{
    public class Circle: Dot
    {
        public double Radius { get; set; }

        public Circle(double x, double y, double radius) : base(x, y)
        {
            Radius = radius;
        }
    }
}
