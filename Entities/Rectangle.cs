﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy.Entities
{
    public class Rectangle: Dot
    {
        public double With { get; set; }
        public double Height { get; set; }
        public Rectangle(double x, double y, double width, double height) : base(x, y)
        {
            With = width;
            Height = height;
        }
    }
}
