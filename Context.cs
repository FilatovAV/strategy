﻿using System;
using System.Collections.Generic;
using System.Text;
using Strategy.Entities;
using Strategy.Interfaces;

namespace Strategy
{
    public class Context: IExport
    {
        private IExport export;

        public Context(IExport export)
        {
            Console.WriteLine(export.ToString());
            this.export = export;
        }
        public string Export(Dot dot)
        {
            return export.Export(dot);
        }
        public string Export(Circle dot)
        {
            return export.Export(dot);
        }
        public string Export(Rectangle dot)
        {
            return export.Export(dot);
        }
    }
}
