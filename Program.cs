﻿using Strategy.Entities;
using Strategy.Interfaces;
using Strategy.Serializers;
using System;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            Dot dot = new Dot(100, 200);
            Circle circle = new Circle(110, 210, 12);
            Rectangle rectangle = new Rectangle(120, 220, 11, 33);

            IExport exportXML = new ExportXML();
            IExport exportJSON = new ExportJSON();

            var context = new Context(exportXML);

            var dotXml = context.Export(dot);
            var circleXml = context.Export(circle);
            var rectangleXml = context.Export(rectangle);

            Console.WriteLine(dotXml);
            Console.WriteLine(circleXml);
            Console.WriteLine(rectangleXml);

            context = new Context(exportJSON);
            var dotJSON = context.Export(dot);
            var circleJSON = context.Export(circle);
            var rectangleJSON = context.Export(rectangle);

            Console.WriteLine(dotJSON);
            Console.WriteLine(circleJSON);
            Console.WriteLine(rectangleJSON);


            Console.ReadLine();
        }
    }
}
