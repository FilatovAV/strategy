﻿using Strategy.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy.Interfaces
{
    public interface IExport
    {
        string Export(Dot dot);
        string Export(Circle circle);
        string Export(Rectangle rectangle);

    }
}
