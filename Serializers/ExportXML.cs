﻿using Strategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Strategy.Entities;

namespace Strategy.Serializers
{
    public class ExportXML: IExport
    {
        public string Export(Dot dot)
        {
            return $@"<DOT>
                        <X>{dot.X}</X>
                        <Y>{dot.Y}</Y>
                    </DOT>";
        }
        public string Export(Rectangle shape)
        {
            return $@"<RECTANGLE>
                        <X>{shape.X}</X>
                        <Y>{shape.Y}</Y>
                        <H>{shape.Height}>/H>
                        <W>{shape.With}</W>
                    </RECTANGLE>";
        }

        public string Export(Circle shape)
        {
            return $@"<CIRCLE>
                        <X>{shape.X}</X>
                        <Y>{shape.Y}</Y>
                        <R>{shape.Radius}>/R>
                    </CIRCLE>";
        }
        public override string ToString()
        {
            return "ExportXML";
        }
    }
}
