﻿using Strategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Strategy.Entities;

namespace Strategy.Serializers
{
    public class ExportJSON: IExport
    {
        public string Export(Dot dot)
        {
            return $@"{{""X"": {dot.X:F1}, ""Y"": {dot.Y:F1}}}";
        }
        public string Export(Rectangle shape)
        {
            return $@"{{""X"": {shape.X:F1}, ""Y"": {shape.Y:F1}, ""Height"": {shape.Height:F1}, ""With"": {shape.With:F1}}}";
        }

        public string Export(Circle shape)
        {
            return $@"{{""X"": {shape.X:F1}, ""Y"": {shape.Y:F1}, ""Radius"": {shape.Radius:F1}}}";
        }
        public override string ToString()
        {
            return "ExportJSON";
        }
    }
}
